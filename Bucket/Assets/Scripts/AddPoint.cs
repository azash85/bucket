﻿using UnityEngine;
using System.Collections;

public class AddPoint : MonoBehaviour {

	int myScore = 0;
	public TextMesh scoreText;
	public static int myHealth = 28;

	void Start () {

		scoreText.text = "Score: " + myScore;
	}

	void OnTriggerEnter2D(Collider2D collisionObject){
		Destroy (collisionObject.gameObject);
		myScore++;

		scoreText.text = "Score: " + myScore;

	}

}
