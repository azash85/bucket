﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour {

	private Vector3 startPosition;
	private float newXPos = -10f;
	public float moveSpeed = 1f;
	public float moveDistance = 4f;

	public int spawnedObjects = 0;
	public TextMesh spawnedObjectsText;

	public GameObject[] randomGOs;

	public float timeLeftUntilSpawn = 0f;
	public float startTime = 0f;
	public float secondsBetweenSpawn = 1f;

	void Start () {

		startPosition = transform.position;

	}

	void SpawnRandomObject(){
		int rndItem = Random.Range (0, 6);
		GameObject rndObj = Instantiate (randomGOs[rndItem]) as GameObject;
		rndObj.transform.position = transform.position;

		//adding force and spin to the spawning objects
		rndObj.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 400f));
		rndObj.GetComponent<Rigidbody2D>().AddTorque(Random.Range(-150f, 150f));
	}
	

	void Update () {

		//Creates a new position depending on Time.time and movespeed from -10 to 10
		newXPos = Mathf.PingPong(Time.time * moveSpeed, moveDistance)- (moveDistance/2);

		//Moves the object to the new position
		transform.position = new Vector3 (newXPos, startPosition.y, startPosition.z);

		timeLeftUntilSpawn = Time.time - startTime;
		if (timeLeftUntilSpawn >= secondsBetweenSpawn) {
			startTime = Time.time - Random.Range(0.1f, 0.5f);
			timeLeftUntilSpawn = 0f;
			SpawnRandomObject();
			spawnedObjects++;
			spawnedObjectsText.text = "Obj: " + spawnedObjects;
		}
	}
}
