﻿using UnityEngine;
using System.Collections;

public class TankMovement : MonoBehaviour {

	public float moveSpeed = 0.5f;
	

	// Update is called once per frame
	void Update () {
		float horiz = Input.GetAxis("Horizontal");
		transform.Translate (new Vector3 (horiz*moveSpeed, 0, 0));
	}
}
